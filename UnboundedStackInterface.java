
public interface UnboundedStackInterface<T> extends StackInterface<Object> {
	void push(T element);

}
