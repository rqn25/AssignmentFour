
import java.awt.TextField;
import java.io.IOException;
import javafx.fxml.FXML;
import javafx.scene.control.PasswordField;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;

public class SignupController {
	@FXML
	TextField uname;
	@FXML
	PasswordField pw;
	@FXML
	PasswordField confirmpw;

public void createAccount() {
	if (pw.getText().equals(confirmpw.getText())) {
	User user = new User(uname.getText(), pw.getText(), null, null, null, null, null, null, null);
	UserDB.getUsers().add(user);
	try {
	UserIO.writeUsers(UserDB.getUsers());
	} catch (IOException e) {
	System.err.println("Cannot write UserDB to binary file.");
	e.printStackTrace();
	}
	} else {
	pw.setStyle("-fx-background-color: slateblue; -fx-text-fill: white; -fx-border-color: red");
	confirmpw.setStyle("-fx-background-color: slateblue; -fx-text-fill: white; -fx-border-color: red");
	}

	System.out.println(UserDB.getUsers());

	}

public void createFileChooser() {
	FileChooser fileChooser = new FileChooser();
	fileChooser.setTitle("Open Resource File");
	fileChooser.getExtensionFilters().addAll(
	new ExtensionFilter("Text Files", "*.txt"),
	new ExtensionFilter("Image Files", "*.png", "*.jpg", "*.gif"),
	new ExtensionFilter("Audio Files", "*.wav", "*.mp3", "*.aac"),
	new ExtensionFilter("All Files", "*.*")
	);

	}

}
