
public class LinkedStack<T> implements UnboundedStackInterface<T> {
	protected LLNode<T> top;

public LinkedStack() {
	top = null;
	}

public void push(T element) {
	LLNode<T> newNode = new LLNode<T>(element);
	newNode.setLink(top);
	top = newNode;
	}

public void pop() {
	if (!isEmpty())
	top = top.getLink();
	else
	throw new StackUnderflowException("Pop attempted on an empty statck.");
	}

public T top() {
	if (!isEmpty())
	return top.getInfo();
	else
	throw new StackUnderflowException("Top attempted on an empty stack.");
	}

public boolean isEmpty() {
	return (top == null);
	}

}
